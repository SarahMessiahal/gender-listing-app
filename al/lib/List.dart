import 'package:al/Boxes/boxes.dart';
import 'package:al/home_page.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:intl/intl.dart';

import 'dbhelp/help.dart';

class ListPage extends StatefulWidget {
  ListPage({super.key, this.dData});
  String? dData;

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  int noOfMaleEmp = 0;
  int noOfFemaleEmp = 0;

  @override
  void initState() {
    calculateNoOfMale();

    super.initState();
  }

  void calculateNoOfMale() {
    List<Dbase> listOfMale = Boxes.getData()
        .values
        .cast<Dbase>()
        .where((element) => element.gender == "Male")
        .toList();

    setState(
      () {
        noOfMaleEmp = listOfMale.length;
        noOfFemaleEmp =
            Boxes.getData().values.toList().length - listOfMale.length;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("Asset"),
        titleTextStyle: TextStyle(
          fontSize: 25.0,
          fontWeight: FontWeight.bold,
          color: Color.fromARGB(255, 1, 24, 83),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 1, 24, 83),
            onPressed: () async {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 35, 115, 243),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(12.0),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            margin: EdgeInsets.all(
              12.0,
            ),
            child: Ink(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: <Color>[
                    Color.fromARGB(255, 53, 189, 239),
                    Color.fromARGB(255, 26, 12, 96),
                  ],
                ),
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    24.0,
                  ),
                ),
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(
                      24.0,
                    ),
                  ),
                ),
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(
                  vertical: 18.0,
                  horizontal: 8.0,
                ),
                child: Column(
                  children: [
                    const Text(
                      'Total Members',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(
                                  20.0,
                                ),
                              ),
                              padding: const EdgeInsets.all(
                                6.0,
                              ),
                              margin: const EdgeInsets.only(
                                right: 8.0,
                              ),
                              child: const Icon(
                                Icons.male,
                                size: 28.0,
                                color: Color.fromARGB(255, 0, 191, 255),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            const Text(
                              "Male",
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              noOfMaleEmp.toString(),
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(
                                  20.0,
                                ),
                              ),
                              padding: const EdgeInsets.all(
                                6.0,
                              ),
                              margin: const EdgeInsets.only(
                                right: 8.0,
                              ),
                              child: const Icon(
                                Icons.female,
                                size: 28.0,
                                color: Color.fromARGB(255, 240, 114, 221),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            const Text(
                              "Female",
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              noOfFemaleEmp.toString(),
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: ValueListenableBuilder<Box<Dbase>>(
              valueListenable: Boxes.getData().listenable(),
              builder: (context, box, _) {
                var data = box.values.toList().cast<Dbase>();
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 12),
                  child: ListView.builder(
                    itemCount: box.length,
                    reverse: true,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Card(
                          color: data[index].gender == "Male"
                              ? Color.fromARGB(255, 0, 191, 255)
                              : Color.fromARGB(255, 240, 114, 221),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 15, horizontal: 10),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      data[index].name.toString(),
                                      style: const TextStyle(
                                        fontSize: 25,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 1, 24, 83),
                                      ),
                                    ),
                                    const Spacer(),
                                    InkWell(
                                      onTap: () {
                                        delete(data[index]);
                                      },
                                      child: const Icon(
                                        Icons.delete,
                                        color: Color.fromARGB(255, 1, 24, 83),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      DateFormat('EE, dd MMMM yyyy').format(
                                        DateTime.parse(
                                          data[index].date.toString(),
                                        ),
                                      ),
                                      style: const TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 1, 24, 83),
                                      ),
                                    ),
                                    const Spacer(),
                                    Icon(
                                      data[index].gender == "Male"
                                          ? Icons.male
                                          : Icons.female,
                                      size: 28.0,
                                      color: data[index].gender == "Male"
                                          ? Color.fromARGB(255, 2, 38, 50)
                                          : Color.fromARGB(255, 73, 1, 62),
                                    ),
                                    Text(
                                      data[index].gender.toString(),
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 1, 24, 83),
                                      ),
                                    ),
                                    const Spacer(),
                                    Text(
                                      data[index].dropdown.toString(),
                                      style: const TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Color.fromARGB(255, 1, 24, 83),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color.fromARGB(255, 1, 24, 83),
        onPressed: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) {
                return HomePage(dData: widget.dData!);
              },
            ),
          ).then((value) => calculateNoOfMale());
        },
        child: Icon(Icons.add),
      ),
    );
  }

  void delete(Dbase dbase) async {
    await dbase.delete();

    calculateNoOfMale();
  }
}
