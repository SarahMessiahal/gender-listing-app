import 'package:al/dbhelp/help.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'Boxes/boxes.dart';

class HomePage extends StatefulWidget {
  HomePage({super.key, this.dData});
  String? dData;

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  DateTime today = DateTime.now();
  DateTime now = DateTime.now();
  int index = 1;
  final _formKey = GlobalKey<FormState>();
  DateTime selectDate = DateTime.now();

  int gender = -1;
  final nameController = TextEditingController();
  final List<String> listItem = [
    "Mouse",
    "PC",
    "Chair",
    "Desk",
    "Bag",
    "Cabin",
  ];
  String? selectedItems;

  DateFormat dateFormat = DateFormat("dd MM yyyy");

  Future<void> _selectDate(BuildContext context) async {
    final DateTime currentDate = DateTime.now();

    DateTime firstDate = currentDate.subtract(const Duration(days: 40 * 365));

    DateTime lastDate = currentDate.subtract(const Duration(days: 18 * 365));

    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: lastDate,
      firstDate: firstDate,
      lastDate: lastDate,
    );
    if (picked != null && picked != selectDate) {
      setState(() {
        selectDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 35, 115, 243),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(
                  25.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "Welcome to Page ${widget.dData!}",
                      style: const TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.w700,
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              TextFormField(
                key: _formKey,
                keyboardType: TextInputType.text,
                controller: nameController,
                decoration: InputDecoration(
                  hintText: 'Employee Name',
                  hintStyle: TextStyle(color: Colors.white),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.white,
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 1.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                validator: (use) {
                  if (use!.isEmpty) {
                    return 'Please enter text';
                  } else if (nameController.text.length < 10) {
                    return "Text Length should be more than 6 ";
                  }

                  return null;
                },
              ),
              SizedBox(
                height: 50.0,
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 0, 191, 255),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        padding: EdgeInsets.all(
                          12.0,
                        ),
                        child: Icon(
                          Icons.male,
                          size: 24.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        width: 12.0,
                      ),
                      ChoiceChip(
                        label: Text(
                          "Male",
                          style: TextStyle(
                            fontSize: 18.0,
                            color: gender == 1 ? Colors.white : Colors.black,
                          ),
                        ),
                        selectedColor: Color.fromARGB(255, 0, 191, 255),
                        onSelected: (val) {
                          if (val) {
                            setState(() {
                              gender = 1;
                            });
                          }
                        },
                        selected: gender == 1 ? true : false,
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 240, 114, 221),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        padding: EdgeInsets.all(
                          12.0,
                        ),
                        child: Icon(
                          Icons.female,
                          size: 24.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      ChoiceChip(
                        label: Text(
                          "Female",
                          style: TextStyle(
                            fontSize: 18.0,
                            color: gender == 2 ? Colors.white : Colors.black,
                          ),
                        ),
                        selectedColor: Color.fromARGB(255, 240, 114, 221),
                        onSelected: (val) {
                          if (val) {
                            setState(
                              () {
                                gender = 2;
                              },
                            );
                          }
                        },
                        selected: gender == 2 ? true : false,
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 50.0,
              ),
              SizedBox(
                height: 50.0,
                child: TextButton(
                  onPressed: () {
                    _selectDate(context);
                    FocusScope.of(context).unfocus();
                  },
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                      EdgeInsets.zero,
                    ),
                  ),
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 1, 24, 83),
                          borderRadius: BorderRadius.circular(
                            16.0,
                          ),
                        ),
                        padding: const EdgeInsets.all(
                          12.0,
                        ),
                        child: const Icon(
                          Icons.date_range,
                          size: 24.0,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        width: 12.0,
                      ),
                      Text(
                        dateFormat.format(selectDate),
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 30.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FormField<String>(
                  builder: (FormFieldState<String> state) {
                    return InputDecorator(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.black, width: 1.0),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: selectedItems,
                          hint: const Text(
                            'Assets',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold),
                          ),
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold),
                          dropdownColor: Color.fromARGB(255, 1, 24, 83),
                          items: listItem.map<DropdownMenuItem<String>>((e) {
                            return DropdownMenuItem<String>(
                              value: e,
                              child: Text(e),
                            );
                          }).toList(),
                          onChanged: (String? val) {
                            setState(
                              () {
                                selectedItems = val!;
                              },
                            );
                          },
                        ),
                      ),
                    );
                  },
                ),
              ),
              TextButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                  ),
                  backgroundColor: MaterialStateProperty.all(
                    Color.fromARGB(255, 1, 24, 83),
                  ),
                ),
                onPressed: () async {
                  setState(
                    () {
                      final box = Boxes.getData();

                      List<Dbase> templist = box.values
                          .cast<Dbase>()
                          .where(
                              (element) => element.name == nameController.text)
                          .toList();

                      if (templist.isEmpty) {
                        var data = Dbase(
                          name: nameController.text,
                          gender: gender == 1 ? "Male" : "Female",
                          date: selectDate,
                          dropdown: selectedItems!,
                        );

                        box.add(data);

                        nameController.clear();
                        Navigator.pop(context);
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            backgroundColor: Color.fromARGB(255, 1, 24, 83),
                            content: Text(
                              "Fill columns!",
                              style: TextStyle(
                                fontSize: 16.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  );
                },
                child: const Text(
                  'Submit',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
