import 'package:hive/hive.dart';

part 'help.g.dart';

@HiveType(typeId: 0)
class Dbase extends HiveObject {
  @HiveField(0)
  String name;

  @HiveField(1)
  String gender;

  @HiveField(2)
  DateTime date;
  @HiveField(3)
  String dropdown;

  Dbase({
    required this.name,
    required this.gender,
    required this.date,
    required this.dropdown,
  });
}
