part of 'help.dart';

class DbhelpAdapter extends TypeAdapter<Dbase> {
  @override
  final int typeId = 0;

  @override
  Dbase read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Dbase(
        name: fields[0] as String,
        gender: fields[1] as String,
        date: fields[2] as DateTime,
        dropdown: fields[3] as String);
  }

  @override
  void write(BinaryWriter writer, Dbase obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.gender)
      ..writeByte(2)
      ..write(obj.date)
      ..writeByte(3)
      ..write(obj.dropdown);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DbhelpAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
